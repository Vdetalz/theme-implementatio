<?php
/**
 * Search Form
 *
 * @package WordPress
 * @subpackage Test_Theme
 * @since Test Theme  1.0
 */
?>

<form class="searchbar" method="get" id="searchform"
      action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<fieldset>
		<div>
			<span class="input_text"><input type="text" class="clearinput"
			                                name="s" id="s"
			                                value="<?php get_search_query(); ?>"/></span>
			<input type="submit" class="input_submit" id="searchsubmit"
			       value=""/>
		</div>
	</fieldset>
</form>
