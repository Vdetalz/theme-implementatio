<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Test_Theme
 * @since 1.0
 * @version 1.2
 */

?>

<ul id="social_icons">
	<li><a href="<?php echo esc_url( get_theme_mod( 'facebook' ) ); ?>" target="_blank">
			<img src="<?php echo esc_url( get_theme_mod( 'icon-facebook' ) ); ?>"></a></li>
	<li><a href="<?php echo esc_url( get_theme_mod( 'linkedin' ) ); ?>" target="_blank">
			<img src="<?php echo esc_url( get_theme_mod( 'icon-linkedin' ) ); ?>"></a></li>
	<li><a href="<?php echo esc_url( get_theme_mod( 'twitter' ) ); ?>" target="_blank">
			<img src="<?php echo esc_url( get_theme_mod( 'icon-twitter' ) ); ?>"></a></li>
	<li><a href="<?php echo esc_url( get_theme_mod( 'rss' ) ); ?>"target="_blank">
			<img src="<?php echo esc_url( get_theme_mod( 'icon-rss' ) ); ?>"></a></li>
</ul>
<!-- #social-navigation -->
