<?php
/**
 * Displays top navigation
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>
<?php
wp_nav_menu( array(
	'menu' => 'main',
	'container' => 'nav',
	'menu_id' => 'top_menu',
	'walker' => new My_Walker_Nav_Menu,
));
?>
<!-- #site-navigation -->
