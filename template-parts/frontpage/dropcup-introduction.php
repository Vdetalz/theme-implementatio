<?php
/**
 * Template-part for front-page
 */

$args      = array(
	'post_type'      => 'post',
	'posts_per_page' => 1,
	'order'          => 'DESC',
	'orderby'        => 'comment_count',
);
$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ) :

	?>
	<div class="introduction">
		<?php
		while ( $the_query->have_posts() ) :
			$the_query->the_post();
			?>

			<h1><?php the_title(); ?></h1>
			<?php the_content( '', '' ); ?>
		<?php
		endwhile;
		wp_reset_postdata();
		?>
		<a class="button button_big button_orange float_left" href="<?php the_permalink(); ?>"><span class="inside"><?php _e( 'read more', 'testtheme' ); ?></span></a>
	</div>

<?php else :
	_e( 'Sorry, no posts were found.', 'testtheme' ); ?><?php echo '<span style="color:red">' . wp_basename( $template ) . '</span>';

endif; ?>
