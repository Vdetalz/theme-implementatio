<?php
/**
 * Template-part for front-page
 */

$args = array(
	'post_type'      => 'portfolio',
	'posts_per_page' => 4,
	'order'          => 'DESC',
	'orderby'        => 'date',
);

$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ) : ?>

<div class="portfolio">
	<p class="all_projects"><a href="<?php echo esc_url( get_post_type_archive_link( 'portfolio' ) ); ?>">
			<?php _e( 'View all projects', 'testtheme' ); ?></a>
	</p>
	<h1>Portfolio</h1>
	<div class="columns">

		<?php
		while ( $the_query->have_posts() ) :
			$the_query->the_post();
			?>
			<div class="column column25">

				<?php
				if ( has_post_thumbnail() ) {
					$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
				?>

				<a href="<?php echo esc_url( $large_image_url[0] ); ?>" class="image lightbox" data-rel="prettyPhoto[gallery]">
								<span class="inside">

									<?php
									the_post_thumbnail( 'front_portfolio' );
				}
									?>
									<span class="caption"><?php the_title(); ?></span>
								</span>
					<span class="image_shadow"></span>
				</a>
			</div>

			<?php
		endwhile;
		wp_reset_postdata();
		else :
			?>
			<h1 class="page_title"><?php _e( 'Sorry, no posts were found.', 'testtheme' ); ?></h1>
			<?php
		endif;
		?>
		<div class="clear"></div>
	</div>
</div>

