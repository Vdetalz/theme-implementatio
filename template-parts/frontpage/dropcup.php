<?php
/**
 * Template-part for front-page
 */

$post_category_top = get_theme_mod( 'posts-options-top' );

$args          = array(
	'post_type'      => 'post',
	'cat'            => $post_category_top,
	'posts_per_page' => 3,
	'order'          => 'DESC',
	'orderby'        => 'date',
);

$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ) :

	get_template_part( 'template-parts/frontpage/dropcup', 'introduction' ); ?>

	<ul class="columns dropcap">

		<?php
		while ( $the_query->have_posts() ) :
			$the_query->the_post();
			$value = get_post_meta( get_the_ID(), 'testtheme_icon', true );
		?>

		<li class="column column33 <?php echo esc_html( $value ); ?>">
			<?php
			$image_attributes = wp_get_attachment_image_src( $value, array( 39, 39 ) );
			$src = $image_attributes[0];
			?>
			<div class="inside" style="background-image: url('<?php echo esc_url( $src ); ?>')">
				<h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

				<?php
				the_excerpt();
				endwhile;
				wp_reset_postdata();
				?>
	</ul>
	<div class="underline"></div>

<?php else : ?>
	<?php _e( 'Sorry, no posts were found.', 'testtheme' ); ?>
<?php endif; ?>
