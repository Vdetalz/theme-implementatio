<?php
/**
 * Template-part for the footer.
 *
 * @package WordPress
 * @subpackage Test_Theme
 * @since Test Theme  1.0
 */

if ( is_active_sidebar( 'sidebar-footer' ) ) { ?>
	<section id="above_footer">
		<div class="wrapper above_footer_boxes page_text">

			<?php dynamic_sidebar( 'sidebar-footer' ); ?>

		</div>
	</section>
<?php
}
