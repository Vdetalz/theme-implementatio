<?php

/**
 * Taxonomy for portfolio
 */
function testtheme_taxonomies_portfolio() {
	$labels = array(
		'name'              => _x( 'Categories Portfolios', 'testtheme' ),
		'singular_name'     => _x( 'Category Portfolios', 'testtheme' ),
		'search_items'      => __( 'Search category Portfolios', 'testtheme' ),
		'all_items'         => __( 'All categories Portfolios', 'testtheme' ),
		'parent_item'       => __( 'Parent category Portfolios', 'testtheme' ),
		'parent_item_colon' => __( 'Parent category Portfolios:', 'testtheme' ),
		'edit_item'         => __( 'Edit category Portfolios', 'testtheme' ),
		'update_item'       => __( 'Update category Portfolios', 'testtheme' ),
		'add_new_item'      => __( 'Add new category Portfolios', 'testtheme' ),
		'new_item_name'     => __( 'New category Portfolios', 'testtheme' ),
		'menu_name'         => __( 'Categories Portfolios', 'testtheme' ),
	);
	$args   = array(
		'labels'       => $labels,
		'hierarchical' => true,
	);
	register_taxonomy( 'portfolio_categories', 'portfolio', $args );
}

add_action( 'init', 'testtheme_taxonomies_portfolio', 0 );

/**
 * Custom content type for portfolio
 */
function register_portfolio_posttype() {
	$labels = array(
		'name'               => _x( 'Portfolio', 'testtheme' ),
		'singular_name'      => _x( 'Portfolio', 'testtheme' ),
		'add_new'            => __( 'Add New', 'testtheme' ),
		'add_new_item'       => __( 'Add New Portfolio', 'testtheme' ),
		'edit_item'          => __( 'Edit Portfolio', 'testtheme' ),
		'new_item'           => __( 'New Portfolio', 'testtheme' ),
		'view_item'          => __( 'See Portfolio', 'testtheme' ),
		'search_items'       => __( 'Search Portfolio', 'testtheme' ),
		'not_found'          => __( 'Portfolio', 'testtheme' ),
		'not_found_in_trash' => __( 'Portfolio', 'testtheme' ),
		'parent_item_colon'  => __( 'Portfolio', 'testtheme' ),
		'menu_name'          => __( 'Portfolios', 'testtheme' ),
	);

	$supports = array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'cat-slides' );

	$post_type_args = array(
		'labels'             => $labels,
		'singular_label'     => __( 'Portfolio', 'testtheme' ),
		'public'             => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'publicly_queryable' => true,
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => true,
		'rewrite'            => array(
			'slug' => 'portfolio',
			'with_front' => false,
		),
		'supports'           => $supports,
		'taxonomies'         => array( 'portfolio_categories' ),
		'menu_position'      => 27,
	);
	register_post_type( 'portfolio', $post_type_args );
}

add_action( 'init', 'register_portfolio_posttype' );

/**
 * Add Metabox for Slides.
 */
$porfolio_metabox = array(
	'id'       => 'porfoliometa',
	'title'    => __( 'Insert values', 'testtheme' ),
	'page'     => array( 'portfolio' ),
	'context'  => 'normal',
	'priority' => 'default',
	'fields'   => array(
		array(
			'name'        => 'Client',
			'desc'        => '',
			'id'          => 'testtheme_client',
			'class'       => 'testtheme_client',
			'type'        => 'text',
			'rich_editor' => 0,
			'max'         => 0,
		),
		array(
			'name'        => 'Model',
			'desc'        => '',
			'id'          => 'testtheme_model',
			'class'       => 'testtheme_model',
			'type'        => 'text',
			'rich_editor' => 0,
			'max'         => 0,
		),
		array(
			'name'        => 'Photographer',
			'desc'        => '',
			'id'          => 'testtheme_photographer',
			'class'       => 'testtheme_photographer',
			'type'        => 'text',
			'rich_editor' => 0,
			'max'         => 0,
		),
	),
);

/**
 * Add metaboxes(custom fields type of events).
 */
function testtheme_add_portfolio_meta_box() {

	global $porfolio_metabox;

	foreach ( $porfolio_metabox['page'] as $page ) {
		add_meta_box( $porfolio_metabox['id'], $porfolio_metabox['title'], 'testtheme_show_portfolio_box', $page, 'normal', 'default', $porfolio_metabox );
	}
}

add_action( 'admin_menu', 'testtheme_add_portfolio_meta_box' );

/**
 * View metabox.
 */
function testtheme_show_portfolio_box() {
	global $post;
	global $porfolio_metabox;
	global $testtheme_prefix;
	global $wp_version;

	echo '<input type="hidden" name="porfolio_metabox_meta_box_nonce" value="', wp_create_nonce( basename( __FILE__ ) ), '" />';

	echo '<table class="form-table">';

	foreach ( $porfolio_metabox['fields'] as $field ) {

		$meta = get_post_meta( $post->ID, $field['id'], true );

		echo '<tr>',
		'<th style="width:20%"><label for="', esc_attr( $field['id'] ), '">', esc_html( $field['name'] ), '</label></th>',
			'<td class="testtheme_field_type_' . str_replace( ' ', '_', $field['type'] ) . '">';
		switch ( $field['type'] ) {
			case 'text':
				echo '<input type="text" name="', esc_attr( $field['id'] ), '" id="', esc_attr( $field['id'] ), '" value="', $meta ? esc_attr( $meta ) : esc_attr( $field['std'] ), '" size="30" style="width:97%" /><br/>', '', esc_html( $field['desc'] );
				break;
		}
		echo '<td>',
		'</tr>';
	}

	echo '</table>';
}

/**
 * Save metaboxes values.
 */
add_action( 'save_post', 'porfolio_save_meta_box' );
function porfolio_save_meta_box( $post_id ) {
	global $post;
	global $porfolio_metabox;

	if ( ! wp_verify_nonce( $_POST['porfolio_metabox_meta_box_nonce'], basename( __FILE__ ) ) ) {
		return $post_id;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post_id;
	}

	if ( 'page' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return $post_id;
		}
	} elseif ( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
	}

	foreach ( $porfolio_metabox['fields'] as $field ) {

		$old = get_post_meta( $post_id, $field['id'], true );
		$new = $_POST[ $field['id'] ];

		if ( $new && $new != $old ) {
			if ( $field['type'] == 'date' ) {
				$new = $new;
				update_post_meta( $post_id, $field['id'], $new );
			} else {
				if ( is_string( $new ) ) {
					$new = $new;
				}
				update_post_meta( $post_id, $field['id'], $new );

			}
		} elseif ( '' == $new && $old ) {
			delete_post_meta( $post_id, $field['id'], $old );
		}
	}
}
