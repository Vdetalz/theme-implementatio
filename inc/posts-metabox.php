<?php
/**
 * File add metabox for the posts.
 *
 * @package WordPress
 * @subpackage Test_Theme
 * @since 1.0
 */

$images_metabox = array(
	'id'       => 'imagemeta',
	'title'    => __( 'Insert Icon', 'testtheme' ),
	'page'     => array( 'post', 'page' ),
	'context'  => 'side',
	'priority' => 'high',
	'fields'   => array(
		array(
			'image'       => 'icon-image',
			'desc'        => __( 'Icon for displaying in front page in png format. Size 39x39px.', 'testtheme' ),
			'id'          => 'testtheme_icon',
			'class'       => 'testtheme_icon',
			'type'        => 'file',
			'rich_editor' => 0,
			'max'         => 0,
		),
	),
);

/**
 * Add theme metabox for the posts.
 */
function add_posts_images_metabox() {
	global $images_metabox;

	foreach ( $images_metabox['page'] as $page ) {
		add_meta_box( $images_metabox['id'], $images_metabox['title'], 'testtheme_show_images_metabox', $page, 'side', 'high', $images_metabox );
	}
}

add_action( 'admin_menu', 'add_posts_images_metabox' );

/**
 * View metabox
 */
function testtheme_show_images_metabox() {
	global $post;
	global $images_metabox;
	global $testtheme_prefix;
	global $wp_version;

	$default = get_stylesheet_directory_uri() . '/img/no-image.png';


	echo '<input type="hidden" name="images_metabox_meta_box_nonce" value="', wp_create_nonce( basename( __FILE__ ) ), '" />';

	echo '<div>';

	foreach ( $images_metabox['fields'] as $field ) {

		$value = get_post_meta( $post->ID, $field['id'], true );

		if ( $value ) {
			$image_attributes = wp_get_attachment_image_src( $value, array(
				39,
				39,
			) );
			$src              = $image_attributes[0];
		} else {
			$src = $default;
		}

		echo '<img data-src="' . esc_url( $default ) . '" src="' . esc_url( $src ) . '" />';

		echo '<div id="minor-publishing-actions">';

		switch ( $field['type'] ) {
			case 'file':
				echo '<input type="hidden" name="' . esc_html( $field['id'] ) . '" id="' . esc_html( $field['id'] ) . '" value="' . esc_html( $value ) . '" />';
				echo '<button type="submit" class="upload_image_button button">' . __( 'Upload', 'testtheme' ) . '</button><button type="submit" class="remove_image_button button">&times;</button>';
				echo '<p>' . esc_html( $field['desc'] ) . '</p>';
				break;
		}
	}
	echo '</div></div>';
}

/**
 * Save metaboxes values.
 *
 * @param int $post_id ID.
 *
 * @return mixed
 */
function save_images_metabox( $post_id ) {
	global $post;
	global $images_metabox;

	if ( ! wp_verify_nonce( $_POST['images_metabox_meta_box_nonce'], basename( __FILE__ ) ) ) {
		return $post_id;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post_id;
	}

	if ( 'page' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return $post_id;
		}
	} elseif ( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
	}

	foreach ( $images_metabox['fields'] as $field ) {

		$old = get_post_meta( $post_id, $field['id'], true );
		$new = $_POST[ $field['id'] ];

		if ( $new && $new != $old ) {
			if ( 'file' == $field['type'] ) {
				$new = $new;
				update_post_meta( $post_id, $field['id'], $new );
			} else {
				if ( is_string( $new ) ) {
					$new = $new;
				}
				update_post_meta( $post_id, $field['id'], $new );

			}
		} elseif ( '' == $new && $old ) {
			delete_post_meta( $post_id, $field['id'], $old );
		}
	}
}
add_action( 'save_post', 'save_images_metabox' );


/**
 * Add script for metabox.
 */
function admin_loader_img() {
	if ( ! did_action( 'wp_enqueue_media' ) ) {
		wp_enqueue_media();
	}
	wp_enqueue_script( 'myuploadscript', get_stylesheet_directory_uri() . '/js/admin.js', array( 'jquery' ), null, false );
}

add_action( 'admin_enqueue_scripts', 'admin_loader_img' );
