<?php
/**
 * File create allfor flexslider:
 * - content type,
 * - taxonomy,
 * - metaboxes,
 * - view slide,
 */

/**
 * Custom content type for slider.
 */
function register_slides_posttype() {
	$labels = array(
		'name'               => _x( 'Slides', 'testtheme' ),
		'singular_name'      => _x( 'Slide', 'testtheme' ),
		'add_new'            => __( 'Add New', 'testtheme' ),
		'add_new_item'       => __( 'Add New slide', 'testtheme' ),
		'edit_item'          => __( 'Edit slide', 'testtheme' ),
		'new_item'           => __( 'New slide', 'testtheme' ),
		'view_item'          => __( 'See slide', 'testtheme' ),
		'search_items'       => __( 'Search slides', 'testtheme' ),
		'not_found'          => __( 'Slide', 'testtheme' ),
		'not_found_in_trash' => __( 'Slide', 'testtheme' ),
		'parent_item_colon'  => __( 'Slide', 'testtheme' ),
		'menu_name'          => __( 'Slides', 'testtheme' ),
	);

	$supports = array( 'title', 'thumbnail', 'cat-slides' );

	$post_type_args = array(
		'labels'             => $labels,
		'singular_label'     => __( 'Slide', 'testtheme' ),
		'public'             => true,
		'show_ui'            => true,
		'publicly_queryable' => true,
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => true,
		'rewrite'            => array(
			'slug' => 'slides',
			'with_front' => false,
		),
		'supports'           => $supports,
		'menu_position'      => 27,
	);
	register_post_type( 'slides', $post_type_args );
}

add_action( 'init', 'register_slides_posttype' );

/**
 * Taxonomy for flexsider
 */
function testtheme_taxonomies_slider() {
	$labels = array(
		'name'              => _x( 'Categories Sides', 'testtheme' ),
		'singular_name'     => _x( 'Category Sides', 'testtheme' ),
		'search_items'      => __( 'Search category slides', 'testtheme' ),
		'all_items'         => __( 'All categories slides', 'testtheme' ),
		'parent_item'       => __( 'Parent category slides', 'testtheme' ),
		'parent_item_colon' => __( 'Parent category slides:', 'testtheme' ),
		'edit_item'         => __( 'Edit category slides', 'testtheme' ),
		'update_item'       => __( 'Update category slides', 'testtheme' ),
		'add_new_item'      => __( 'Add new category slides', 'testtheme' ),
		'new_item_name'     => __( 'New category slides', 'testtheme' ),
		'menu_name'         => __( 'Categories slides', 'testtheme' ),
	);
	$args   = array(
		'labels'       => $labels,
		'hierarchical' => true,
	);
	register_taxonomy( 'cat-slides', 'slides', $args );
}

add_action( 'init', 'testtheme_taxonomies_slider', 0 );

/**
 * Add Metabox for Slides.
 */
$slidelink_2_metabox = array(
	'id'       => 'slidelink',
	'title'    => __( 'Link to slide', 'testtheme' ),
	'page'     => array( 'slides' ),
	'context'  => 'normal',
	'priority' => 'default',
	'fields'   => array(
		array(
			'name'        => 'URL for slide',
			'desc'        => '',
			'id'          => 'testtheme_slideurl',
			'class'       => 'testtheme_slideurl',
			'type'        => 'text',
			'rich_editor' => 0,
			'max'         => 0,
		),
		array(
			'name'        => 'Caption',
			'desc'        => '',
			'id'          => 'testtheme_slidecaption',
			'class'       => 'testtheme_slidecaption',
			'type'        => 'text',
			'rich_editor' => 0,
			'max'         => 0,
		),
		array(
			'name'        => 'Description',
			'desc'        => '',
			'id'          => 'testtheme_slidedesc',
			'class'       => 'testtheme_slidedesc',
			'type'        => 'text',
			'rich_editor' => 0,
			'max'         => 0,
		),
	),
);

/**
 * Add metabox.
 */
function testtheme_add_slidelink_2_meta_box() {

	global $slidelink_2_metabox;

	foreach ( $slidelink_2_metabox['page'] as $page ) {
		add_meta_box( $slidelink_2_metabox['id'], $slidelink_2_metabox['title'], 'testtheme_show_slidelink_2_box', $page, 'normal', 'default', $slidelink_2_metabox );
	}
}

add_action( 'admin_menu', 'testtheme_add_slidelink_2_meta_box' );

/**
 * View metabox.
 */
function testtheme_show_slidelink_2_box() {
	global $post;
	global $slidelink_2_metabox;
	global $testtheme_prefix;
	global $wp_version;

	echo '<input type="hidden" name="testtheme_slidelink_2_meta_box_nonce" value="', wp_create_nonce( basename( __FILE__ ) ), '" />';

	echo '<table class="form-table">';

	foreach ( $slidelink_2_metabox['fields'] as $field ) {

		$meta = get_post_meta( $post->ID, $field['id'], true );

		echo '<tr>',
		'<th style="width:20%"><label for="', $field['id'], '">', esc_html( $field['name'] ), '</label></th>',
			'<td class="testtheme_field_type_' . esc_attr( str_replace( ' ', '_', $field['type'] ) ) . '">';
		switch ( $field['type'] ) {
			case 'text':
				echo '<input type="text" name="', esc_attr( $field['id'] ), '" id="', esc_attr( $field['id'] ), '" value="', $meta ? esc_attr( $meta ) : esc_attr( $field['std'] ), '" size="30" style="width:97%" /><br/>', '', esc_html( $field['desc'] );
				break;
		}
		echo '<td>',
		'</tr>';
	}

	echo '</table>';
}

/**
 * Save metabox data.
 *
 * @param int $post_id ID.
 *
 * @return mixed
 */
function testtheme_slidelink_2_save( $post_id ) {
	global $post;
	global $slidelink_2_metabox;

	if ( ! wp_verify_nonce( $_POST['testtheme_slidelink_2_meta_box_nonce'], basename( __FILE__ ) ) ) {
		return $post_id;
	}

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return $post_id;
	}

	if ( 'page' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return $post_id;
		}
	} elseif ( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
	}

	foreach ( $slidelink_2_metabox['fields'] as $field ) {

		$old = get_post_meta( $post_id, $field['id'], true );
		$new = $_POST[ $field['id'] ];

		if ( $new && $new !== $old ) {
			if ( 'date' === $field['type'] ) {
				$new = $new;
				update_post_meta( $post_id, $field['id'], $new );
			} else {
				if ( is_string( $new ) ) {
					$new = $new;
				}
				update_post_meta( $post_id, $field['id'], $new );

			}
		} elseif ( '' == $new && $old ) {
			delete_post_meta( $post_id, $field['id'], $old );
		}
	}
}
add_action( 'save_post', 'testtheme_slidelink_2_save' );

/**
 * Function implements the output of the slider.
 */
function testtheme_slider_template( $idc ) {

	$args = array(
		'post_type'      => 'slides',
		'posts_per_page' => 5,
		'tax_query'      => array(
			array(
				'taxonomy' => 'cat-slides',
				'field'    => 'slug',
				'terms'    => $idc,
			),
		),
	);

	$the_query = new WP_Query( $args );

	if ( $the_query->have_posts() ) {
		?>
		<!-- BEGIN TOP -->
		<section id="top">
			<div class="wrapper">
				<div id="top_slide" class="flexslider">
					<ul class="slides">

						<?php
						while ( $the_query->have_posts() ) :
								$the_query->the_post();
							?>
							<li>
								<?php
								if ( has_post_thumbnail() ) {
									the_post_thumbnail();
								}
								if ( get_post_meta( get_the_id(), 'testtheme_slidecaption', true ) != '' ) { ?>

									<p class="flex-caption">
										<strong><?php esc_html_e( get_post_meta( get_the_id(), 'testtheme_slidecaption', true ) ); ?></strong>

										<?php
										if ( get_post_meta( get_the_id(), 'testtheme_slidedesc', true ) !== '' ) {
											?>

											<span><?php esc_html_e( get_post_meta( get_the_id(), 'testtheme_slidedesc', true ) ); ?></span>
										<?php } ?>
									</p>

								<?php } ?>
							</li>
						<?php endwhile; ?>

					</ul><!-- .slides -->
				</div><!-- .flexslider -->
			</div>
		</section>
		<!-- END TOP -->
	<?php
	}
	wp_reset_postdata();
}
