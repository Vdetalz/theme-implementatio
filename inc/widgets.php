<?php
/**
 * Test Theme widgets.
 *
 * @package WordPress
 * @subpackage Test_Theme
 * @since 1.0
 */

/**
 * Add widget - "theme_recent_posts_widget"
 */
function theme_recent_posts_register_widget() {
	register_widget( 'Theme_Recent_Posts_Widget' );
}

add_action( 'widgets_init', 'theme_recent_posts_register_widget' );

/**
 * Class Theme_Recent_Posts_Widget
 */
class Theme_Recent_Posts_Widget extends WP_Widget {
	/**
	 * Theme_Recent_Posts_Widget constructor.
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'   => 'theme_widget_class',
			'description' => __( 'Widget For Displaying Recent Porfolio', 'testtheme' ),
		);
		$this->WP_Widget( 'Theme_Recent_Posts_Widget', __( 'Recent Portfolio', 'testtheme' ), $widget_ops );
	}

	/**
	 * Form.
	 *
	 * @param array $instance curent instance.
	 *
	 * @return form
	 */
	public function form( $instance ) {
		$defaults = array(
			'title'     => __( 'Recent Posts', 'testtheme' ),
			'num_posts' => '3',
		);

		$instance  = wp_parse_args( (array) $instance, $defaults );
		$title     = $instance['title'];
		$num_posts = $instance['num_posts'];
		?>
		<p><?php esc_html_e( 'Title:', 'testtheme' ); ?>
			<input class="widefat" name="<?php esc_attr_e( $this->get_field_name( 'title' ) ); ?>"
			       type="text" value="<?php esc_attr_e( $title, 'testtheme' ); ?>"/></p>
		<p><?php _e( 'Count Recent Posts:', 'testtheme' ) ?>
			<input class="widefat" name="<?php esc_attr_e( $this->get_field_name( 'num_posts' ) ); ?>"
			       type="text" value="<?php esc_attr_e( $num_posts ); ?>"/>
		</p>
		<?php
	}

	/**
	 * Update.
	 *
	 * @param array $new_instance new content.
	 * @param array $old_instance old content.
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance              = $old_instance;
		$instance['title']     = sanitize_text_field( $new_instance['title'] );
		$instance['num_posts'] = trim( strip_tags( $new_instance['num_posts'] ) );

		return $instance;
	}

	/**
	 * Widget.
	 *
	 * @param array $args args.
	 * @param array $instance current instance.
	 */
	public function widget( $args, $instance ) {
		global $post;

		extract( $args );
		echo $before_widget;
		$title     = apply_filters( 'widget_title', $instance['title'] );
		$num_posts = ( empty( $instance['num_posts'] ) ) ? '' : $instance['num_posts'];

		if ( ! empty( $title ) ) {
			echo $before_title . esc_html( $title ) . $after_title;
		}

		$args = array(
			'posts_per_page'      => absint( $num_posts ),
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			//'category_name'         => 'blog',
			'ignore_sticky_posts' => true,
		);

		$num_posts = new WP_Query( $args );

		if ( $num_posts->have_posts() ) {

			while ( $num_posts->have_posts() ) :
				$num_posts->the_post();
				?>
				<ul class="recent_posts">
					<li class="item">
						<a class="thumbnail" href="#">
							<?php the_post_thumbnail( array( 50, 50 ) ); ?>
						</a>
						<div class="text">
							<?php
							$the_excerpt = get_the_excerpt();
							$string            = substr( $the_excerpt, 0, 100 );
							?>
							<h4 class="title"><a href="<?php the_permalink(); ?>"><?php echo esc_html( $string ); ?></a></h4>
							<p class="data">
								<span class="date"><?php the_date( 'd/m/Y' ); ?></span>
							</p>
						</div>
					</li>
				</ul>

			<?php
			endwhile;
			wp_reset_postdata();
		} else {
			?>
			<p><?php esc_html_e( 'Not Found', 'testtheme' ); ?><p>;
			<?php
		}
		echo $after_widget;
	}
}

/**
 * Add widget - "theme_category_widget"
 */
function theme_category_register_widget() {
	register_widget( 'Theme_Category_Widget' );
}

add_action( 'widgets_init', 'theme_category_register_widget' );

/**
 * Class theme_category_widget
 */
class Theme_Category_Widget extends WP_Widget {

	/**
	 * Theme_Category_Widget constructor.
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'                   => 'widget_theme_categories',
			'description'                 => __( 'A list or dropdown of categories.' ),
			'customize_selective_refresh' => true,
		);
		$this->WP_Widget( 'Theme_Category_Widget', __( 'Category Widget', 'testtheme' ), $widget_ops );
	}

	/**
	 * Form.
	 *
	 * @param array $instance instance.
	 *
	 * @return form.
	 */
	function form( $instance ) {
		// Defaults.
		$instance     = wp_parse_args( (array) $instance, array(
			'title' => '',
		) );
		$title        = sanitize_text_field( $instance['title'] );
		$count        = isset( $instance['count'] ) ? (bool) $instance['count'] : false;
		$hierarchical = isset( $instance['hierarchical'] ) ? (bool) $instance['hierarchical'] : false;
		$dropdown     = isset( $instance['dropdown'] ) ? (bool) $instance['dropdown'] : false;
	?>
	<p><label for="<?php esc_html_e( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Title:', 'testtheme' ); ?></label>
		<input class="widefat" id="<?php esc_html_e( $this->get_field_id( 'title' ) ); ?>"
		       name="<?php esc_html_e( $this->get_field_name( 'title' ) ); ?>" type="text"
		       value="<?php esc_html_e( esc_attr( $title ) ); ?>"/></p>

	<p><input type="checkbox" class="checkbox" id="<?php esc_html_e( $this->get_field_id( 'dropdown' ) ); ?>"
	          name="<?php esc_html_e( $this->get_field_name( 'dropdown' ) ); ?>"<?php checked( $dropdown ); ?> />
		<label
			for="<?php esc_html_e( $this->get_field_id( 'dropdown' ) ); ?>"><?php _e( 'Display as dropdown', 'testtheme' ); ?></label><br/>

		<input type="checkbox" class="checkbox" id="<?php esc_html_e( $this->get_field_id( 'count' ) ); ?>"
		       name="<?php esc_html_e( $this->get_field_name( 'count' ) ); ?>"<?php checked( $count ); ?> />
		<label
			for="<?php esc_html_e( $this->get_field_id( 'count' ) ); ?>"><?php _e( 'Show post counts', 'testtheme' ); ?></label><br/>

		<input type="checkbox" class="checkbox" id="<?php esc_html_e( $this->get_field_id( 'hierarchical' ) ); ?>"
		       name="<?php esc_html_e( $this->get_field_name( 'hierarchical' ) ); ?>"<?php checked( $hierarchical ); ?> />
		<label
			for="<?php esc_html_e( $this->get_field_id( 'hierarchical' ) ); ?>"><?php _e( 'Show hierarchy', 'testtheme' ); ?></label>
	</p>
	<?php
}

	/**
	 * Update.
	 *
	 * @param array $new_instance instance.
	 * @param array $old_instance old instance.
	 *
	 * @return array
	 */
	function update( $new_instance, $old_instance ) {
		$instance                 = $old_instance;
		$instance['title']        = sanitize_text_field( $new_instance['title'] );
		$instance['count']        = ! empty( $new_instance['count'] ) ? 1 : 0;
		$instance['hierarchical'] = ! empty( $new_instance['hierarchical'] ) ? 1 : 0;
		$instance['dropdown']     = ! empty( $new_instance['dropdown'] ) ? 1 : 0;

		return $instance;
	}

	/**
	 * Widget.
	 *
	 * @param array $args args.
	 * @param array $instance instance.
	 */
	function widget( $args, $instance ) {
		static $first_dropdown = true;

		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Categories' ) : $instance['title'], $instance, $this->id_base );

		$c = ! empty( $instance['count'] ) ? '1' : '0';
		$h = ! empty( $instance['hierarchical'] ) ? '1' : '0';
		$d = ! empty( $instance['dropdown'] ) ? '1' : '0';

		echo $args['before_widget'];
		if ( $title ) {
			echo $args['before_title'] . esc_html( $title ) . $args['after_title'];
		}

		$cat_args = array(
			'orderby'      => 'name',
			'taxonomy'     => 'portfolio_categories',
			'show_count'   => $c,
			'hierarchical' => $h,
			'walker'       => new Walker_Theme_Category,
		);

		if ( $d ) {
			$dropdown_id    = ( $first_dropdown ) ? 'portfolio_categories' : "{$this->id_base}-dropdown-{$this->number}";
			$first_dropdown = false;

			echo '<label class="screen-reader-text" for="' . esc_attr( $dropdown_id ) . '">' . esc_html( $title ) . '</label>';

			$cat_args['show_option_none'] = __( 'Select Category' );
			$cat_args['id']               = $dropdown_id;

			/**
			 * Filters the arguments for the Categories widget drop-down.
			 *
			 * @since 2.8.0
			 *
			 * @see wp_dropdown_categories()
			 *
			 * @param array $cat_args An array of Categories widget drop-down arguments.
			 */
			wp_dropdown_categories( apply_filters( 'widget_categories_dropdown_args', $cat_args ) );
			?>

			<script type='text/javascript'>
				/* <![CDATA[ */
				(function () {
					var dropdown = document.getElementById("<?php echo esc_js( $dropdown_id ); ?>");

					function onCatChange() {
						if (dropdown.options[dropdown.selectedIndex].value > 0) {
							location.href = "<?php echo home_url(); ?>/?cat=" + dropdown.options[dropdown.selectedIndex].value;
						}
					}

					dropdown.onchange = onCatChange;
				})();
				/* ]]> */
			</script>

			<?php
		} else {
			?>
			<ul class="menu categories page_text">
				<?php
				$cat_args['title_li'] = '';

				/**
				 * Filters the arguments for the Categories widget.
				 *
				 * @since 2.8.0
				 *
				 * @param array $cat_args An array of Categories widget options.
				 */
				wp_list_categories( apply_filters( 'widget_categories_args', $cat_args ) );
				?>
			</ul>
			<?php
		}

		echo $args['after_widget'];
	}
}

/**
 * Custom Walker_Category for the taxonomy - portfolio_category
 */
class Walker_Theme_Category extends Walker_Category {

	/**
	 * Custom start output.
	 *
	 * @param string $output output data.
	 * @param object $category category.
	 * @param int $depth depth.
	 * @param array $args args.
	 * @param int $id ID.
	 */
	function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
		/** This filter is documented in wp-includes/category-template.php */
		$cat_name = apply_filters(
			'list_cats',
			esc_attr( $category->name ),
			$category
		);

		// Don't generate an element if the category name is empty.
		if ( ! $cat_name ) {
			return;
		}

		$link = '<a href="' . esc_url( get_term_link( $category ) ) . '" ';
		if ( $args['use_desc_for_title'] && ! empty( $category->description ) ) {
			/**
			 * Filters the portfolio_category description for display.
			 *
			 * @since 1.2.0
			 *
			 * @param string $description portfolio_category description.
			 * @param object $category portfolio_category object.
			 */
			$link .= 'title="' . esc_attr( strip_tags( apply_filters( 'category_description', $category->description, $category ) ) ) . '"';
		}

		$link .= '>';
		$link .= $cat_name . ' (' . number_format_i18n( $category->count ) . ')</a>';

		if ( ! empty( $args['feed_image'] ) || ! empty( $args['feed'] ) ) {
			$link .= ' ';

			if ( empty( $args['feed_image'] ) ) {
				$link .= '(';
			}

			$link .= '<a href="' . esc_url( get_term_feed_link( $category->term_id, $category->taxonomy, $args['feed_type'] ) ) . '"';

			if ( empty( $args['feed'] ) ) {
				$alt = ' alt="' . sprintf( __( 'Feed for all posts filed under %s' ), $cat_name ) . '"';
			} else {
				$alt  = ' alt="' . $args['feed'] . '"';
				$name = $args['feed'];
				$link .= empty( $args['title'] ) ? '' : $args['title'];
			}

			$link .= '>';


			if ( empty( $args['feed_image'] ) ) {
				$link .= $name;
			} else {
				$link .= "<img src='" . $args['feed_image'] . "'$alt" . ' />';
			}

			if ( ! empty( $args['show_count'] ) ) {
				$link .= ' (' . number_format_i18n( $category->count ) . ')';//
			}

			$link .= '</a>';

			if ( empty( $args['feed_image'] ) ) {
				$link .= ')';
			}
		}

		if ( 'list' == $args['style'] ) {
			$output .= "\t<li";
			$css_classes = array(
				'cat-item',
				'cat-item-' . $category->term_id,
			);

			if ( ! empty( $args['current_category'] ) ) {
				// 'current_category' can be an array, so we use `get_terms()`.
				$_current_terms = get_terms( $category->taxonomy, array(
					'include'    => $args['current_category'],
					'hide_empty' => false,
				) );

				foreach ( $_current_terms as $_current_term ) {
					if ( $category->term_id == $_current_term->term_id ) {
						$css_classes[] = 'current-cat';
					} elseif ( $category->term_id == $_current_term->parent ) {
						$css_classes[] = 'current-cat-parent';
					}
					while ( $_current_term->parent ) {
						if ( $category->term_id == $_current_term->parent ) {
							$css_classes[] = 'current-cat-ancestor';
							break;
						}
						$_current_term = get_term( $_current_term->parent, $category->taxonomy );
					}
				}
			}

			/**
			 * Filters the list of CSS classes to include with each category in the list.
			 *
			 * @since 4.2.0
			 *
			 * @see wp_list_categories()
			 *
			 * @param array $css_classes An array of CSS classes to be applied to each list item.
			 * @param object $category Category data object.
			 * @param int $depth Depth of page, used for padding.
			 * @param array $args An array of wp_list_categories() arguments.
			 */
			$css_classes = implode( ' ', apply_filters( 'category_css_class', $css_classes, $category, $depth, $args ) );

			$output .= ' class="' . $css_classes . '"';
			$output .= ">$link\n";
		} elseif ( isset( $args['separator'] ) ) {
			$output .= "\t$link" . $args['separator'] . "\n";
		} else {
			$output .= "\t$link<br />\n";
		}
	}

	/**
	 * @param string $output
	 * @param object $category
	 * @param int $depth
	 * @param array $args
	 */
	function end_el( &$output, $category, $depth = 0, $args = array() ) {
		$output .= "</a></li>\n";
	}
}

/**
 * Add widget - "theme_contact_widget"
 */
add_action( 'widgets_init', 'theme_contact_register_widget' );
function theme_contact_register_widget() {
	register_widget( 'theme_contact_widget' );
}

/**
 * Class theme_contact_widget
 */
class theme_contact_widget extends WP_Widget {
	/**
	 * theme_recent_posts_widget constructor.
	 */
	public function __construct() {
		$widget_ops = array(
			'classname'   => 'theme_widget_class',
			'description' => __( 'Widget For Displaying Contacts', 'testtheme' )
		);
		$this->WP_Widget( 'theme_contact_widget', __( 'Contacts', 'testtheme' ), $widget_ops );
	}

	/**
	 * @param array $instance
	 *
	 * @return form
	 */
	public function form( $instance ) {
		$defaults = array(
			'title'         => __( 'Contact Us', 'testtheme' ),
			'theme_tel1'    => '',
			'theme_tel2'    => '',
			'theme_mail'    => '',
			'theme_address' => '',
		);

		$instance = wp_parse_args( ( array ) $instance, $defaults );
		$title    = $instance['title'];
		$tel1     = $instance['theme_tel1'];
		$tel2     = $instance['theme_tel2'];
		$mail     = $instance['theme_mail'];
		$address  = $instance['theme_address'];
		?>
		<p><?php _e( 'Title:', 'testtheme' ); ?>
			<input class="widefat"
			       name="<?php echo $this->get_field_name( 'title' ); ?>"
			       type="text" value="<?php esc_attr_e( $title, 'testtheme' ); ?>"/></p>
		<p><?php _e( 'Phone 1:', 'testtheme' ) ?>
			<input class="widefat" name="<?php echo $this->get_field_name( 'theme_tel1' ); ?>"
			       type="text" value="<?php esc_attr_e( $tel1 ); ?>"/>
		</p>
		<p><?php _e( 'Phone2:', 'testtheme' ); ?>
			<input class="widefat"
			       name="<?php echo $this->get_field_name( 'theme_tel2' ); ?>"
			       type="text" value="<?php esc_attr_e( $tel2, 'testtheme' ); ?>"/></p>
		<p><?php _e( 'Mail:', 'testtheme' ); ?>
			<input class="widefat"
			       name="<?php echo $this->get_field_name( 'theme_mail' ); ?>"
			       type="email" value="<?php esc_attr_e( $mail, 'testtheme' ); ?>"/></p>
		<p><?php _e( 'Location:', 'testtheme' ); ?>
			<input class="widefat" id="edit-location"
			       name="<?php echo $this->get_field_name( 'theme_address' ); ?>"
			       type="text" value="<?php esc_attr_e( $address, 'testtheme' ); ?>" placeholder="Anything you want!"/>
		</p>

		<?php
	}

	/**
	 * @param array $new_instance
	 * @param array $old_instance
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance                  = $old_instance;
		$instance['title']         = sanitize_text_field( $new_instance['title'] );
		$instance['theme_tel1']    = sanitize_text_field( $new_instance['theme_tel1'] );
		$instance['theme_tel2']    = sanitize_text_field( $new_instance['theme_tel2'] );
		$instance['theme_mail']    = sanitize_text_field( $new_instance['theme_mail'] );
		$instance['theme_address'] = sanitize_text_field( $new_instance['theme_address'] );

		return $instance;
	}

	/**
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		global $post;

		extract( $args );
		echo $before_widget;
		$title   = apply_filters( 'widget_title', $instance['title'] );
		$tel1    = $instance['theme_tel1'];
		$tel2    = $instance['theme_tel2'];
		$mail    = $instance['theme_mail'];
		$address = $instance['theme_address'];

		if ( ! empty( $title ) ) {
			echo $before_title . esc_html_e( $title, 'testtheme' ) . $after_title;
		}
		?>
		<ul class="list_contact page_text">
			<li class="phone"><?php echo $tel1; ?><br/><?php echo $tel2; ?></li>
			<li class="email"><a href="#"><?php echo $mail; ?></a></li>
			<li class="address"><?php echo $address; ?></li>
		</ul>
		<?php
		echo $after_widget; ?>
		<?php
	}
}