<?php
/**
 * File add customizer options to the Test Theme.
 *
 * @package WordPress
 * @subpackage Test_Theme
 * @since 1.0
 */

/**
 * Customizer.
 */
function add_theme_customizer() {
	add_theme_page( __( 'Options', 'testtheme' ), __( 'Set Option', 'testtheme' ), 'edit_theme_options', 'customize.php' );
}

add_action( 'admin_menu', 'add_theme_customizer' );

/**
 * Customizer View function.
 *
 * @param object $customizer object of the Customize Manager class.
 */
function register_theme_customizer( $customizer ) {
	$default = 'http://';

	$post_categories = get_categories(
		array(
			'type'       => 'post',
			'child_of'   => 0,
			'orderby'    => 'name',
			'order'      => 'ASC',
			'hide_empty' => 1,
			'taxonomy'   => 'category',
		)
	);

	$choices = array();

	foreach ( $post_categories as $category ) {
		$choices[ $category->term_id ] = $category->name;
	}

	/**
	 * Social data section.
	 */
	$customizer->add_section(
		'social_links',
		array(
			'title'       => __( 'Social links', 'testtheme' ),
			'description' => __( 'Add urls to link with social.', 'testtheme' ),
			'priority'    => 11,
		)
	);

	/**
	 * Show in header.
	 */
	$customizer->add_setting(
		'show_social',
		array(
			'default' => FALSE,
		)
	);

	$customizer->add_control(
		'show_social',
		array(
			'type'    => 'checkbox',
			'label'   => __( 'Show in header', 'testtheme' ),
			'section' => 'social_links',
		)
	);

	/**
	 * Facebook
	 */
	$customizer->add_setting(
		'facebook',
		array(
			'default' => $default,
		)
	);
	$customizer->add_control(
		'facebook',
		array(
			'label'   => __( 'Facebook Url:', 'testtheme' ),
			'section' => 'social_links',
			'type'    => 'text',
		)
	);
	$customizer->add_setting(
		'icon-facebook'
	);
	$customizer->add_control(
		new WP_Customize_Image_Control(
			$customizer,
			'icon-facebook',
			array(
				'label'    => __( 'Upload icon:', 'testtheme' ),
				'section'  => 'social_links',
				'settings' => 'icon-facebook',
			)
		)
	);

	/**
	 * Twitter.
	 */
	$customizer->add_setting(
		'twitter',
		array(
			'default' => $default,
		)
	);
	$customizer->add_control(
		'twitter',
		array(
			'label'   => __( 'Twitter Url:', 'testtheme' ),
			'section' => 'social_links',
			'type'    => 'text',
		)
	);
	$customizer->add_setting(
		'icon-twitter'
	);
	$customizer->add_control(
		new WP_Customize_Image_Control(
			$customizer,
			'icon-twitter',
			array(
				'label'    => __( 'Upload icon:', 'testtheme' ),
				'section'  => 'social_links',
				'settings' => 'icon-twitter',
			)
		)
	);

	/**
	 * Linkedin.
	 */
	$customizer->add_setting(
		'linkedin',
		array(
			'default' => $default,
		)
	);
	$customizer->add_control(
		'linkedin',
		array(
			'label'   => __( 'Linkedin Url:', 'testtheme' ),
			'section' => 'social_links',
			'type'    => 'text',
		)
	);
	$customizer->add_setting(
		'icon-linkedin'
	);
	$customizer->add_control(
		new WP_Customize_Image_Control(
			$customizer,
			'icon-linkedin',
			array(
				'label'    => __( 'Upload icon:', 'testtheme' ),
				'section'  => 'social_links',
				'settings' => 'icon-linkedin',
			)
		)
	);

	/**
	 * Rss.
	 */
	$customizer->add_setting(
		'rss',
		array(
			'default' => $default,
		)
	);
	$customizer->add_control(
		'rss',
		array(
			'label'   => __( 'Rss Url:', 'testttheme' ),
			'section' => 'social_links',
			'type'    => 'text',
		)
	);
	$customizer->add_setting(
		'icon-rss'
	);
	$customizer->add_control(
		new WP_Customize_Image_Control(
			$customizer,
			'icon-rss',
			array(
				'label'    => __( 'Upload icon:', 'testtheme' ),
				'section'  => 'social_links',
				'settings' => 'icon-rss',
			)
		)
	);

	/**
	 * Logo Section.
	 */
	$customizer->add_section(
		'site_logo_section',
		array(
			'title'       => __( 'Site Logo', 'testtheme' ),
			'description' => __( 'Add logo image.', 'testtheme' ),
			'priority'    => 11,
		)
	);
	$customizer->add_setting(
		'site-logo'
	);
	$customizer->add_control(
		new WP_Customize_Image_Control(
			$customizer,
			'site-logo',
			array(
				'label'    => __( 'Upload logo:', 'testtheme' ),
				'section'  => 'site_logo_section',
				'settings' => 'site-logo',
			)
		)
	);

	/**
	 * Displaying posts on the main page on section top.
	 */
	$customizer->add_section(
		'posts_section',
		array(
			'title'       => __( 'Posts Options', 'testtheme' ),
			'description' => __( 'Displaying posts on the main page.', 'testtheme' ),
			'priority'    => 11,
		)
	);
	$customizer->add_setting(
		'posts-options-top'
	);
	$customizer->add_control(
		'posts-options-top',
		array(
			'label'   => __( 'Category on top:', 'testttheme' ),
			'section' => 'posts_section',
			'type'    => 'radio',
			'choices' => $choices,
		)
	);
	/**
	 * Displaying posts on the main page on section bottom.
	 */
	$customizer->add_setting(
		'posts-options-bottom'
	);
	$customizer->add_control(
		'posts-options-bottom',
		array(
			'label'   => __( 'Category on bottom:', 'testttheme' ),
			'section' => 'posts_section',
			'type'    => 'radio',
			'choices' => $choices,
		)
	);
}

add_action( 'customize_register', 'register_theme_customizer' );
