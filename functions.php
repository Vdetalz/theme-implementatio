<?php
/**
 * Test Theme only works in WordPress 4.7 or later.
 *
 * @package WordPress
 * @subpackage Test_Theme
 * @since 1.0
 */

/**
 * Locale
 *
 * @param string $locale curernt locale.
 *
 * @return string|void
 */
function testtheme_localized( $locale ) {
	if ( isset( $_GET['lang'] ) ) {
		return esc_attr( $_GET['lang'] );
	}

	return $locale;
}

add_filter( 'locale', 'testtheme_localized' );

if ( ! function_exists( 'testtheme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 *
	 * @since Test Theme 1.0
	 */
	function testtheme_setup() {
		/**
		 * Translation.
		 */
		load_theme_textdomain( 'vitaliy', get_template_directory() . '/languages' );

		/**
		 * Add support tumbnails for custom content-type.
		 */
		add_theme_support( 'post-thumbnails' );

		/**
		 * Add image size:
		 * -197x140, for frontpage portfolio
		 * -209x136,
		 * -262x88,
		 * -267x230, for portfolio archive
		 * -446x294, for gallery
		 * -689x214, for blog
		 * -686x380,
		 * -606x480, for portfolio single slider
		 */
		add_image_size( 'front_portfolio', 197, 140, TRUE );
		add_image_size( 'some', 209, 136, TRUE );
		add_image_size( 'some1', 262, 88, TRUE );
		add_image_size( 'archive_portfolio', 267, 230, TRUE );
		add_image_size( 'gallery_portfolio', 446, 294, TRUE );
		add_image_size( 'blog_portfolio', 689, 214, TRUE );
		add_image_size( 'some3', 686, 380, TRUE );
		add_image_size( 'single_portfolio_slider', 606, 480, TRUE );

		/**
		 * Add support post formats.get
		 */
		add_theme_support( 'post-formats', array(
			'aside',
			'gallery',
			'link',
			'image',
			'quote',
			'status',
			'video',
			'chat',
			'audio',
		) );

	}
endif;
add_action( 'after_setup_theme', 'testtheme_setup' );

/**
 * The file implements the output of the slider.
 */
include_once get_template_directory() . '/inc/flexslider.php';

include_once get_template_directory() . '/inc/portfolio.php';
include_once get_template_directory() . '/inc/widgets.php';
/**
 * Custom metabox for the posts.
 */
include_once get_template_directory() . '/inc/posts-metabox.php';
/**
 * Customizer.
 */
include_once get_template_directory() . '/inc/theme-customizer.php';

/**
 * Registered menu.
 */
add_theme_support( 'nav-menus' );
if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus( array(
		'main' => 'Main Nav',
	) );
}

/**
 * Changes to styles and wrappers of menu items. Insert div class="submenu".
 *
 * Class My_Walker_Nav_Menu
 */
class My_Walker_Nav_Menu extends Walker_Nav_Menu {

	/**
	 * Add classes to ul sub-menus.
	 *
	 * @param string $output output content menu item.
	 * @param int $depth depth of the sub menu items.
	 */
	function start_lvl( &$output, $depth ) {
		// depth dependent classes.
		$indent        = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' );
		$display_depth = ( $depth + 1 );
		$classes       = array(
			'sub-menu',
			( $display_depth % 2 ? 'menu-odd' : 'menu-even' ),
			( $display_depth >= 2 ? 'sub-sub-menu' : '' ),
			'menu-depth-' . $display_depth,
		);
		$class_names   = implode( ' ', $classes );

		// build html.
		$output .= "\n" . '<div class="submenu">' . $indent . '<ul>' . "\n";
	}

	/**
	 * Starts the element output.
	 *
	 * @since 3.0.0
	 * @since 4.4.0 The {@see 'nav_menu_item_args'} filter was added.
	 *
	 * @see Walker::start_el()
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param WP_Post $item Menu item data object.
	 * @param int $depth Depth of menu item. Used for padding.
	 * @param stdClass $args An object of wp_nav_menu() arguments.
	 * @param int $id Current item ID.
	 */
	public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
			$t = '';
			$n = '';
		} else {
			$t = "\t";
			$n = "\n";
		}
		$indent = ( $depth ) ? str_repeat( $t, $depth ) : '';

		$classes   = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . $item->ID;

		$args = apply_filters( 'nav_menu_item_args', $args, $item, $depth );

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args, $depth ) );
		$class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args, $depth );
		$id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

		$output .= $indent . '<li' . $id . $class_names . '>';

		$atts           = array();
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target ) ? $item->target : '';
		$atts['rel']    = ! empty( $item->xfn ) ? $item->xfn : '';
		$atts['href']   = ! empty( $item->url ) ? $item->url : '';

		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args, $depth );

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		$title = apply_filters( 'the_title', $item->title, $item->ID );
		$title = apply_filters( 'nav_menu_item_title', $title, $item, $args, $depth );

		$item_output = $args->before;
		$item_output .= '<a' . $attributes . '><span>';
		$item_output .= $args->link_before . $title . $args->link_after;
		$item_output .= '</a></span>';
		$item_output .= $args->after;

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

}

/**
 * Enqueue scripts and styles.
 *
 * @since Test Theme 1.0
 */
function testtheme_slider_scripts() {
	wp_enqueue_style( 'reset-style', get_template_directory_uri() . '/css/reset.css' );
	wp_enqueue_style( 'style-style', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'light-style', get_template_directory_uri() . '/css/light.css' );
	wp_enqueue_style( 'dark-style', get_template_directory_uri() . '/css/dark.css' );

	wp_style_add_data( 'dark-style', 'alt', TRUE );
	wp_style_add_data( 'dark-style', 'title', 'dark' );
	wp_style_add_data( 'light-style', 'title', 'light' );

	wp_enqueue_style( 'flex-style', get_template_directory_uri() . '/css/flexslider.css' );
	wp_enqueue_style( 'prettyPhoto-style', get_template_directory_uri() . '/css/prettyPhoto.css' );
	wp_enqueue_style( 'testtheme-ie', get_template_directory_uri() . '/css/ie7.css', array( 'style-style' ), '20141010' );
	wp_style_add_data( 'testtheme-ie', 'conditional', 'lt IE 7' );

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'jquery-min', get_template_directory_uri() . '/js/jquery.min.js', array( 'jquery' ), NULL, FALSE );
	wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/js/jquery.ui.min.js', array( 'jquery-min' ), NULL, FALSE );
	wp_enqueue_script( 'flex-script', get_template_directory_uri() . '/js/jquery.flexslider.min.js', array( 'jquery-ui' ), NULL, FALSE );
	wp_enqueue_script( 'prettyphoto-script', get_template_directory_uri() . '/js/jquery.prettyphoto.min.js', array( 'flex-script' ), NULL, FALSE );

	if ( is_post_type_archive( 'portfolio' ) || is_tax( 'portfolio_categories' ) ) {
		wp_enqueue_script( 'quicksand-script', get_template_directory_uri() . '/js/jquery.quicksand.js', array( 'prettyphoto-script' ), NULL, FALSE );
	}
	wp_enqueue_script( 'stylesheettoggle-script', get_template_directory_uri() . '/js/jquery.stylesheettoggle.js', array( 'prettyphoto-script' ), NULL, FALSE );
	wp_enqueue_script( 'onload-script', get_template_directory_uri() . '/js/onload.js', array( 'stylesheettoggle-script' ), NULL, FALSE );
	wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/js/custom-script.js', array( 'jquery' ), NULL, FALSE );
	wp_enqueue_script( 'testtheme-html5', get_template_directory_uri() . '/js/html5.js', array( 'jquery' ), '20141010' );
	wp_script_add_data( 'testtheme-html5', 'conditional', 'IE' );

}

add_action( 'wp_enqueue_scripts', 'testtheme_slider_scripts' );

/**
 * Change the excerpts lenght.
 *
 * @param int $length length of the excerpt.
 *
 * @return int
 */
function new_excerpt_length( $length ) {
	return 20;
}

add_filter( 'excerpt_length', 'new_excerpt_length' );

/**
 * Custom Excerpts text.
 *
 * @param string $more text link.
 *
 * @return string
 */
function excerpt_readmore( $more ) {
	global $post;

	return '<p class="read_more"><a href="' . get_permalink( $post->ID ) . '" class="readmore">' . __( 'Read more' ) . '</a></p>';
}

add_filter( 'excerpt_more', 'excerpt_readmore' );

/**
 * The breadcrumbs for portfolio pages.
 */
function portfolio_breadcrumbs() {

	/* === Options === */
	$text['home']     = get_bloginfo();
	$text['category'] = '%s';
	$text['search']   = __( 'Result search "%s"', 'testtheme' );
	$text['tag']      = __( 'Posts with tag "%s"', 'testtheme' );
	$text['author']   = __( 'Author\'s %s', 'testtheme' );
	$text['404']      = __( 'Error 404', 'testtheme' );
	$text['page']     = __( 'Page %s', 'testtheme' );
	$text['cpage']    = __( 'Comments %s', 'testtheme' );

	$wrap_before = '<div class="breadcrumbs"><div class="inside">';
	$wrap_after  = '</div></div><!-- .breadcrumbs -->';
	//$sep = '›';
	//$sep_before = '<span class="sep">';
	//$sep_after = '</span>';
	$show_home_link = 1;
	$show_on_home   = 0;
	$show_current   = 1;
	$before         = '<a class="last"><span>';
	$after          = '</span></a>';
	/* === END OPTIONS === */

	global $post;
	$home_url = home_url( '/' );
	//$link_before = '<span>';
	//$link_after = '</span>';
	$link_attr      = ' itemprop="item"';
	$link_in_before = '<span>';
	$link_in_after  = '</span>';
	$link           = '<a class="" href="%1$s"' . $link_attr . '>' . $link_in_before . '%2$s' . $link_in_after . '</a>';
	$frontpage_id   = get_option( 'page_on_front' );
	$parent_id      = ( $post ) ? $post->post_parent : '';
	$sep            = ' ' . $sep_before . $sep . $sep_after . ' ';
	$home_link      = '<a href="' . $home_url . '"' . $link_attr . ' class="home">' . $link_in_before . $text['home'] . $link_in_after . '</a>';

	if ( is_home() || is_front_page() ) {

		if ( $show_on_home ) {
			echo $wrap_before . $home_link . $wrap_after;
		}
	} else {

		echo $wrap_before;
		if ( $show_home_link ) {
			echo $home_link;
		}

		if ( is_category() ) {
			$cat = get_category( get_query_var( 'cat' ), FALSE );
			if ( 0 !== $cat->parent ) {
				$cats = get_category_parents( $cat->parent, TRUE, $sep );
				$cats = preg_replace( "#^(.+)$sep$#", '$1', $cats );
				$cats = preg_replace( '#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr . '>' . $link_in_before . '$2' . $link_in_after . '</a>' . $link_after, $cats );
				if ( $show_home_link ) {
					echo $sep;
				}
				echo $cats;
			}
			if ( get_query_var( 'paged' ) ) {
				$cat = $cat->cat_ID;
				echo $sep . sprintf( $link, get_category_link( $cat ), get_cat_name( $cat ) ) . $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_current ) {
					echo $sep . $before . sprintf( $text['category'], single_cat_title( '', FALSE ) ) . $after;
				}
			}

		} elseif ( is_search() ) {
			if ( have_posts() ) {
				if ( $show_home_link && $show_current ) {
					echo $sep;
				}
				if ( $show_current ) {
					echo $before . sprintf( $text['search'], get_search_query() ) . $after;
				}
			} else {
				if ( $show_home_link ) {
					echo $sep;
				}
				echo $before . sprintf( $text['search'], get_search_query() ) . $after;
			}

		} elseif ( is_day() ) {
			if ( $show_home_link ) {
				echo $sep;
			}
			echo sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) . $sep;
			echo sprintf( $link, get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ), get_the_time( 'F' ) );
			if ( $show_current ) {
				echo $sep . $before . get_the_time( 'd' ) . $after;
			}

		} elseif ( is_month() ) {
			if ( $show_home_link ) {
				echo $sep;
			}
			echo sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) );
			if ( $show_current ) {
				echo $sep . $before . get_the_time( 'F' ) . $after;
			}

		} elseif ( is_year() ) {
			if ( $show_home_link && $show_current ) {
				echo $sep;
			}
			if ( $show_current ) {
				echo $before . get_the_time( 'Y' ) . $after;
			}

		} elseif ( is_single() && ! is_attachment() ) {
			if ( $show_home_link ) {
				echo $sep;
			}
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object( get_post_type() );
				$slug      = $post_type->rewrite;
				printf( $link, $home_url . 'index.php/' . $slug['slug'] . '/', $post_type->labels->singular_name );
				if ( $show_current ) {
					echo $sep . $before . get_the_title() . $after;
				}
			} else {
				$cat  = get_the_category();
				$cat  = $cat[0];
				$cats = get_category_parents( $cat, TRUE, $sep );
				if ( ! $show_current || get_query_var( 'cpage' ) ) {
					$cats = preg_replace( "#^(.+)$sep$#", "$1", $cats );
				}
				$cats = preg_replace( '#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr . '>' . $link_in_before . '$2' . $link_in_after . '</a>' . $link_after, $cats );
				echo $cats;
				if ( get_query_var( 'cpage' ) ) {
					echo $sep . sprintf( $link, get_permalink(), get_the_title() ) . $sep . $before . sprintf( $text['cpage'], get_query_var( 'cpage' ) ) . $after;
				} else {
					if ( $show_current ) {
						echo $before . get_the_title() . $after;
					}
				}
			}

			// custom post type
		} elseif ( ! is_single() && ! is_page() && get_post_type() != 'post' && ! is_404() ) {
			$post_type = get_post_type_object( get_post_type() );
			if ( get_query_var( 'paged' ) ) {
				echo $sep . sprintf( $link, get_post_type_archive_link( $post_type->name ), $post_type->label ) . $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_current ) {
					echo $sep . $before . $post_type->label . $after;
				}
			}

		} elseif ( is_attachment() ) {
			if ( $show_home_link ) {
				echo $sep;
			}
			$parent = get_post( $parent_id );
			$cat    = get_the_category( $parent->ID );
			$cat    = $cat[0];
			if ( $cat ) {
				$cats = get_category_parents( $cat, TRUE, $sep );
				$cats = preg_replace( '#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr . '>' . $link_in_before . '$2' . $link_in_after . '</a>' . $link_after, $cats );
				echo $cats;
			}
			printf( $link, get_permalink( $parent ), $parent->post_title );
			if ( $show_current ) {
				echo $sep . $before . get_the_title() . $after;
			}

		} elseif ( is_page() && ! $parent_id ) {
			if ( $show_current ) {
				echo $sep . $before . get_the_title() . $after;
			}

		} elseif ( is_page() && $parent_id ) {
			if ( $show_home_link ) {
				echo $sep;
			}
			if ( $parent_id != $frontpage_id ) {
				$breadcrumbs = array();
				while ( $parent_id ) {
					$page = get_page( $parent_id );
					if ( $parent_id != $frontpage_id ) {
						$breadcrumbs[] = sprintf( $link, get_permalink( $page->ID ), get_the_title( $page->ID ) );
					}
					$parent_id = $page->post_parent;
				}
				$breadcrumbs = array_reverse( $breadcrumbs );
				for ( $i = 0; $i < count( $breadcrumbs ); $i ++ ) {
					echo $breadcrumbs[ $i ];
					if ( $i != count( $breadcrumbs ) - 1 ) {
						echo $sep;
					}
				}
			}
			if ( $show_current ) {
				echo $sep . $before . get_the_title() . $after;
			}

		} elseif ( is_tag() ) {
			if ( get_query_var( 'paged' ) ) {
				$tag_id = get_queried_object_id();
				$tag    = get_tag( $tag_id );
				echo $sep . sprintf( $link, get_tag_link( $tag_id ), $tag->name ) . $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_current ) {
					echo $sep . $before . sprintf( $text['tag'], single_tag_title( '', FALSE ) ) . $after;
				}
			}

		} elseif ( is_author() ) {
			global $author;
			$author = get_userdata( $author );
			if ( get_query_var( 'paged' ) ) {
				if ( $show_home_link ) {
					echo $sep;
				}
				echo sprintf( $link, get_author_posts_url( $author->ID ), $author->display_name ) . $sep . $before . sprintf( $text['page'], get_query_var( 'paged' ) ) . $after;
			} else {
				if ( $show_home_link && $show_current ) {
					echo $sep;
				}
				if ( $show_current ) {
					echo $before . sprintf( $text['author'], $author->display_name ) . $after;
				}
			}

		} elseif ( is_404() ) {
			if ( $show_home_link && $show_current ) {
				echo $sep;
			}
			if ( $show_current ) {
				echo $before . $text['404'] . $after;
			}

		} elseif ( has_post_format() && ! is_singular() ) {
			if ( $show_home_link ) {
				echo $sep;
			}
			echo get_post_format_string( get_post_format() );
		}

		echo $wrap_after;

	}
}

/**
 * Add classes for the list of portfolio categories.
 *
 * @param $css_classes
 * @param $category
 * @param $depth
 * @param $args
 *
 * @return array
 */
function filter_function_name( $css_classes, $category, $depth, $args ) {

	if ( $category->taxonomy = 'portfolio_categories' ) {

		foreach ( $css_classes as $class_p ) {

			if ( $class_p == 'cat-item' ) {
				$css_classes[] = 'segment-' . $category->term_taxonomy_id;
			}

			if ( $class_p == 'current-cat' ) {
				$css_classes[] = 'active';
			}
		}

	}

	return $css_classes;
}

add_filter( 'category_css_class', 'filter_function_name', 10, 4 );

/**
 * Function extracts attached image.
 *
 * @param $id_post
 *
 * @return string
 */
function get_attached_imag( $id_post ) {

	$attachment_image = get_children( array(
		'numberposts'    => 1,
		'post_mime_type' => 'image',
		'post_parent'    => $id_post,
		'post_type'      => 'attachment'
	) );

	$attachment_image = array_shift( $attachment_image );

	$img = wp_get_attachment_image( $attachment_image->ID, array(
		276,
		230
	), TRUE );

	return $img;

}

/**
 * Function extracts url from atached image.
 *
 * @param $id_post
 *
 * @return false|string
 */
function get_attached_url( $id_post ) {

	$attachment_image = get_children( array(
		'numberposts'    => 1,
		'post_mime_type' => 'image',
		'post_parent'    => $id_post,
		'post_type'      => 'attachment'
	) );

	$attachment_image = array_shift( $attachment_image );

	$url = wp_get_attachment_url( $attachment_image->ID );

	return $url;

}

/**
 * Register Sidebar Area
 */
function register_theme_widgets() {
	register_sidebar( array(
		'name'          => __( 'Sidebar theme rigth', 'testtheme' ),
		'id'            => "sidebar-theme",
		'description'   => __( 'This panel will be visible on the blog archive page.', 'testtheme' ),
		'class'         => '',
		'before_widget' => '<div class="padd16bot">',
		'after_widget'  => "</div>\n",
		'before_title'  => '<h1>',
		'after_title'   => "</h1>\n",
	) );

	register_sidebar( array(
		'name'          => __( 'Sidebar theme footer', 'testtheme' ),
		'id'            => "sidebar-footer",
		'description'   => __( 'This panel will be visible on the footer.', 'testtheme' ),
		'class'         => '',
		'before_widget' => '<div class="box">',
		'after_widget'  => "</div>\n",
		'before_title'  => '<h3>',
		'after_title'   => "</h3>\n",
	) );
}

add_action( 'widgets_init', 'register_theme_widgets' );

/**
 * Get categories taxonomy portfolio_categories
 */
function get_tax_portfolio() {
	$output = '';
	$args   = array(
		'taxonomy'     => array( 'portfolio_categories' ),
		'orderby'      => 'id',
		'order'        => 'ASC',
		'hide_empty'   => TRUE,
		'number'       => '5',
		'fields'       => 'all',//name
		'count'        => FALSE,
		'hierarchical' => FALSE,
		'child_of'     => 0,
		'cache_domain' => 'core',
	);

	$categories = get_terms( $args );

	if ( $categories && ! is_wp_error( $categories ) ) {

		$output .= '<ul id="portfolio_categories" class="portfolio_categories">';
		$output .= '<li class="active segment-0"><a href="#" class="all">';
		$output .= __( 'All Categories', 'testtheme' );
		$output .= '</a></li>';

		foreach ( $categories as $category ) {
			$output .= '<li class="segment-1"><a href="#" class="' . strtolower( $category->name ) . '">' . $category->name . '</a></li>';
		}

		$output .= '</ul>';
	}

	return $output;
}

/**
 * @param $term_id
 *
 * @return string
 */
function get_term_name( $term_id ) {
	$term_id = (int) $term_id;
	$term    = get_the_terms( $term_id, 'portfolio_categories' );

	if ( ! $term || is_wp_error( $term ) ) {
		return '';
	}

	return $term[0]->name;
}

/**
 * Custom pagination HTML.
 *
 * @param int $range
 */
function testtheme_pagination( $pages = '', $range = 3 ) {
	$showitems = ( $range * 2 ) + 1;

	global $paged;

	if ( empty( $paged ) ) {
		$paged = 1;
	}

	if ( $pages == '' ) {
		global $wp_query;
		$pages = $wp_query->max_num_pages;

		if ( ! $pages ) {
			$pages = 1;
		}
	}

	if ( 1 != $pages ) {
		echo '<ul class="pagenav">';

		if ( $paged > 1 && $showitems < $pages ) {
			echo '<li class="arrow arrow_left"><a href="' . get_pagenum_link( $paged - 1 ) . '"><span> &lt </span></a></li>';
		}

		for ( $i = 1; $i <= $pages; $i ++ ) {

			if ( 1 != $pages && ( ! ( $i >= $paged + $range + 1 || $i <= $paged - $range - 1 ) || $pages <= $showitems ) ) {
				echo ( $paged == $i ) ? '<li class="active"><a href="' . get_pagenum_link( $i ) . '"><span>' . $i . '</span></a></li>' : '<li><a href="' . get_pagenum_link( $i ) . '"><span>' . $i . '</span></a></li>';
			}
		}

		if ( $paged < $pages && $showitems < $pages ) {
			echo '<li class="arrow arrow_right"><a href="' . get_pagenum_link( $paged + 1 ) . '"><span> &gt </span></a></li>';
		}
		echo '</ul>';
	}
}
