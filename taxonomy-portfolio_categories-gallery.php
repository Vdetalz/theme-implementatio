<?php
/**
 * Page for the taxonomy portfolio_categories-gallery
 *
 * @package WordPress
 * @subpackage Test_Theme
 * @since Test Theme  1.0
 */

get_header();
$count = 1;

if ( have_posts() ) : ?>

	<!-- BEGIN CONTENT -->
	<section id="content">
		<div class="wrapper page_text">
			<h1 class="page_title"><?php echo ucfirst( esc_html( get_post_type() ) ); ?></h1>

			<?php
			if ( function_exists( 'portfolio_breadcrumbs' ) ) {
				portfolio_breadcrumbs();
			}
			?>

			<div class="page_gallery">

				<?php
				while ( have_posts() ) : the_post();

					if ( 0 !== $count % 2 ) {
						?>
						<div class="columns">
					<?php } ?>

					<div class="column column50">
						<div class="image">

							<?php the_post_thumbnail( 'gallery_portfolio' ); ?>

							<p class="caption">
								<strong><?php the_post_thumbnail_caption(); ?></strong>

								<?php $excerpt = get_the_excerpt( get_the_ID() ); ?>

								<span><?php esc_html_e( $excerpt ); ?></span>

								<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>

								<a href="<?php echo esc_url( $large_image_url[0] ); ?>"
								   data-rel="prettyPhoto[gallery]"
								   class="button button_small button_orange float_right lightbox"><span
										class="inside"><?php _e( 'zoom', 'testtheme' ) ?></span></a>
							</p>
						</div>
					</div>

					<?php if ( 0 === $count % 2 ) { ?>
						</div>
						<?php
					}

					$count ++;
				endwhile;
				?>

			</div>
		</div>
		</div>
	</section>
	<?php
	testtheme_pagination( get_query_var( 'max_num_pages' ) );

else :
	?>
	<h1 class="page_title"><?php _e( 'Sorry, no posts were found.', 'testtheme' ); ?></h1>
	<?php
endif;

get_footer();
