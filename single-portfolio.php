<?php
/**
 * Page for the single post_type portfolio
 *
 * @package WordPress
 * @subpackage Test_Theme
 * @since Test Theme  1.0
 */
get_header();

if ( have_posts() ) :
	while ( have_posts() ) :
		the_post(); ?>

		<section id="content">
			<div class="wrapper page_text">

				<?php the_title( '<h1 class="page_title">', '</h1>' ); ?>
				<?php
				if ( function_exists( 'portfolio_breadcrumbs' ) ) {
					portfolio_breadcrumbs();
				}
				?>

				<div class="columns">
					<div class="column column33">
						<h1><?php the_title(); ?></h1>
						<?php the_content(); ?>

						<h1><?php _e( 'Client:' ); ?></h1>
						<p><?php echo get_post_meta( get_the_ID(), 'testtheme_client', TRUE ); ?></p>
						<h1><?php ( 'Model & Photographer:' ); ?></h1>
						<p>
							<a href="#"><?php echo get_post_meta( get_the_ID(), 'testtheme_model', TRUE ); ?></a>
							// <?php echo get_post_meta( get_the_ID(), 'testtheme_photographer', TRUE ); ?>
						</p>
					</div>
					<div class="column column66">
						<div id="content_slide">
							<div class="flexslider">
								<ul class="slides">

									<?php $medias = get_attached_media( 'image', get_the_ID() ); ?>

									<?php foreach ( $medias as $media ) { ?>
										<li>
											<a href="<?php echo esc_url( $media->guid ); ?>"
											   class="lightbox"
											   data-rel="prettyPhoto[gallery]"><?php the_post_thumbnail( 'single_portfolio_slider' ); ?></a>
										</li>
									<?php } ?>

								</ul>
							</div>
						</div>
					</div>
				</div>
		</section>
		<!-- END CONTENT -->
		<?php
	endwhile;
else :
	?>
	<h1 class="page_title"><?php _e( 'Sorry, no posts were found.', 'testtheme' ); ?></h1>
	<?php
endif;

get_footer();
