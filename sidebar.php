<?php
/**
 * Right Sidebar .
 *
 * @package WordPress
 * @subpackage Test_Theme
 * @since Test Theme  1.0
 */

?>

<div class="column column25">

	<?php
	if ( function_exists( 'dynamic_sidebar' ) ) {
		dynamic_sidebar( 'sidebar-theme' );
	}
	?>

</div><!-- #sidebar-theme -->
