<?php
/**
 * Page for the archive post_type portfolio
 *
 * @package WordPress
 * @subpackage Test_Theme
 * @since Test Theme  1.0
 */

get_header();

if ( have_posts() ) : ?>

	<section id="content">
		<div class="wrapper page_text">
			<h1 class="page_title"><?php echo ucfirst( esc_html( get_post_type() ) ); ?></h1>

			<?php
			if ( function_exists( 'portfolio_breadcrumbs' ) ) {
				portfolio_breadcrumbs();
			}
			?>
			<?php echo get_tax_portfolio(); ?>
			<div class="portfolio_items_container">
				<ul class="portfolio_items columns">
					<!-- BEGIN CONTENT -->
					<?php
					while ( have_posts() ) :
						the_post();

						$term_name = get_term_name( get_the_ID() );
						?>

						<li data-type="<?php echo lcfirst( $term_name ); ?>"
						    data-id="id-<?php the_ID(); ?>"
						    class="column column33">
							<?php
							if ( has_post_thumbnail() ) {
							$large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
							?>

							<a href="<?php echo esc_url( $large_image_url[0] ); ?>"
							   data-rel="prettyPhoto[gallery]"
							   class="portfolio_image lightbox">
								<div class="inside">

									<?php
									the_post_thumbnail( 'archive_portfolio' );
									}
									?>
									<div class="mask"></div>
								</div>
							</a>
							<h1><?php the_post_thumbnail_caption(); ?></h1>
							<?php the_excerpt(); ?>
							<a class="button button_small button_orange"
							   href="<?php the_permalink(); ?>"><span
									class="inside">read more</span></a>
						</li>

					<?php endwhile; ?>

				</ul>
			</div>
		</div>
	</section>

	<?php
	testtheme_pagination( get_query_var( 'max_num_pages' ) );

else :
	?>
	<h1 class="page_title"><?php _e( 'Sorry, no posts were found.', 'testtheme' ); ?></h1>
	<?php
endif;

get_footer();
