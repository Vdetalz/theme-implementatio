<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Test Theme
 * @since Test Theme 1.0
 */

get_header(); ?>

<section id="content">
	<div class="wrapper page_text page_home">

		<?php if ( have_posts() ) : ?>

			<ul class="columns dropcap">

				<?php
				while ( have_posts() ) :
				the_post();
				?>

				<li class="#">
					<div class="inside">
						<h1>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h1>

						<?php the_content(); ?>
						<?php
						endwhile;

						testtheme_pagination( get_query_var( 'max_num_pages' ) );
						?>

			</ul>
			<div class="underline"></div>

		<?php else : ?>
			<?php _e( 'Sorry, no posts were found.', 'testtheme' ); ?><?php echo '<span style="color:red">' . wp_basename( $template ) . '</span>'; ?>
			</ul>
		<?php endif; ?>

	</div>
</section>

<?php get_footer(); ?>
