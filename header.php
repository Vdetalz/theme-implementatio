<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Test_Theme
 * @since Test Theme 1.0
 */

$site_logo   = get_theme_mod( 'site-logo' );
$show_social = get_theme_mod( 'show_social' );
?>
<!DOCTYPE HTML>
<html <?php language_attributes(); ?>class="no-js no-svg">
<head>
	<title><?php echo wp_get_document_title(); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<!-- BEGIN STYLESHEET SWITCHER -->
<div id="stylesheet_switcher">
	<a href="#" id="switcher"></a>
	<ul id="stylesheets">
		<li>
			<a href="#" class="sheet" id="light">
				<span class="image"><img
						src="<?php bloginfo( 'template_url' ); ?>/gfx/stylesheet_light.jpg"
						alt=""/></span>
				<span class="mask"></span>
				<span
					class="name"><?php _e( 'Light version', 'testtheme' ); ?></span>
			</a>
		</li>
		<li>
			<a href="#" class="sheet" id="dark">
				<span class="image"><img
						src="<?php bloginfo( 'template_url' ); ?>/gfx/stylesheet_dark.jpg"
						alt=""/></span>
				<span class="mask"></span>
				<span
					class="name"><?php _e( 'Dark version', 'testtheme' ); ?></span>
			</a>
		</li>
	</ul>
</div>
<!-- END STYLESHEET SWITCHER -->

<!-- BEGIN PAGE -->
<div id="page">
	<div id="page_top">
		<div id="page_top_in">
			<!-- BEGIN TITLEBAR -->
			<header id="titlebar">
				<div class="wrapper">
					<?php
					if ( '' !== $site_logo ) {
						?>
						<a id="logo" href="<?php echo home_url( '/' ); ?>"><img
								src="<?php echo esc_url( $site_logo ); ?>"/></a>
						<?php
					}
					?>

					<div id="titlebar_right">

						<?php
						if ( TRUE === $show_social ) :
							?>
							<?php get_template_part( 'template-parts/navigation/social', 'main' ); ?>
						<?php endif; ?>

						<div class="clear"></div>

						<?php if ( has_nav_menu( 'main' ) ) : ?>
							<?php get_template_part( 'template-parts/navigation/navigation', 'main' ); ?>
						<?php endif; ?>

					</div>
					<div class="clear"></div>
				</div>
			</header>
			<!-- END TITLEBAR -->
