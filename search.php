<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Test_Theme
 * @since 1.0
 * @version 1.0
 */

get_header();
?>
	<section id="content" xmlns="http://www.w3.org/1999/html">
		<div class="wrapper page_text">
			<?php
			if ( have_posts() ) :
				?>

				<h1 class="page_title"><?php single_cat_title(); ?></h1>

				<?php
				if ( function_exists( 'portfolio_breadcrumbs' ) ) {
					portfolio_breadcrumbs();
				}
				?>

				<div class="columns">

					<?php
					while ( have_posts() ) : the_post();
						?>

						<article class="article">
							<div class="article_image nomargin">
								<div class="inside">

									<?php the_post_thumbnail( 'blog_portfolio' ); ?>

								</div>
							</div>
							<div class="article_details">
								<ul class="article_author_date">
									<li>
										<em><?php _e( 'Add: ', 'testtheme' ); ?></em><?php the_date( 'd.m.Y' ); ?>
									</li>
									<li>
										<em><?php _e( 'Author: ', 'testtheme' ); ?></em><?php the_author(); ?>
									</li>
								</ul>
								<p class="article_comments">
									<em><?php _e( 'Comment: ', 'testtheme' ); ?></em><?php comments_number(); ?>
								</p>
							</div>
							<h1><?php the_title(); ?></h1>
							<?php the_content(); ?>
							<a class="button button_small button_orange"
							   href="<?php the_permalink(); ?>"><span
									class="inside"><?php _e( 'read more', 'testtheme' ) ?></span></a>
						</article>

					<?php endwhile; ?>

				</div>

				<?php
				testtheme_pagination( get_query_var( 'max_num_pages' ) );
			else :
				?>
				<h1 class="page_title"><?php _e( 'Sorry, no posts were found.', 'testtheme' ); ?></h1>
				<?php
			endif;
			?>
			<div>
	</section>
<?php
get_footer();
