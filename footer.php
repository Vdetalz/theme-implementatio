<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Test_Theme
 * @since Test Theme  1.0
 */
?>
<!-- END CONTENT -->
</div>
</div>

<div id="page_bottom">
	<!-- BEGIN ABOVE_FOOTER -->
	<?php if ( is_active_sidebar( 'sidebar-footer' ) ) : ?>
		<?php get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>
		<!-- END ABOVE_FOOTER -->
	<?php endif ?>

	<!-- BEGIN FOOTER -->
	<footer id="footer">
		<div class="wrapper">
			<p class="copyrights"><?php _e( 'Copyright &copy; 2012 by TheSame. All rights reserved.', 'testtheme' ); ?></p>
			<a href="<?php bloginfo( 'url' ); ?>" class="up">
				<span class="arrow"></span>
				<span class="text">top</span>
			</a>
		</div>
	</footer>
	<!-- END FOOTER -->

</div>
</div>

<?php wp_footer(); ?>

</body>
</html>