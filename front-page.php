<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 *
 * @package WordPress
 * @subpackage Test_Theme
 * @since 1.0
 * @version 1.0
 */

get_header();
testtheme_slider_template( 'main' ); ?>

	<!-- BEGIN CONTENT -->
	<section id="content">
		<div class="wrapper page_text page_home">

			<?php get_template_part( 'template-parts/frontpage/dropcup' ); ?>
			<?php get_template_part( 'template-parts/frontpage/iconcup' ); ?>

			<div class="underline"></div>

			<?php get_template_part( 'template-parts/frontpage/portfolio' ); ?>

		</div>
	</section>

<?php get_footer(); ?>

