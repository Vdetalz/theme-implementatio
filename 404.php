<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Test_Theme
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

	<section id="content">
		<div class="wrapper page_text page_home">
			<header class="page-header">
				<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'testtheme' ); ?></h1>
			</header><!-- .page-header -->
			<div class="page-content">
				<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'testtheme' ); ?></p>

				<?php get_search_form(); ?>

			</div><!-- .page-content -->
		</div><!-- #primary -->
	</section><!-- .wrap -->

<?php get_footer();
